/**
 * Created by richardcj on 12/8/17.
 */
import React, { Component } from 'react';



export class CounteButton extends Component{
    constructor(...args){
        super(...args);
        this.state = {
            count: 0
        }
    }

    sumar(){
        this.setState({ count : this.state.count + 1});
    }
    restar(){
        this.setState({ count: this.state.count - 1});
    }

    render(){
        return (
            <div>
                <button onClick={this.sumar.bind(this)}>+</button>
                <button onClick={this.restar.bind(this)}>-</button>
                <span>Contador: {this.state.count}</span>
            </div>
        );
    }

}
