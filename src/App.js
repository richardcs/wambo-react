import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {CounteButton} from './component/count';

class Title extends  Component{

    render(){
        return (
            <h1>{this.props.mensaje}</h1>
        );
    }
}


class App extends Component {
  constructor(...args){
     super(...args);
     this.state = {
         mensaje: 'Esto en un mesaje inicial'
     }
  }
  changeTitle() {
      this.setState({
          mensaje: 'Cambio en el mensaje :)'
      });
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
            <Title mensaje={this.state.mensaje}/>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
          <button onClick={this.changeTitle.bind(this)}>Cambiar de titulo </button>
          <h2>Welcome to React</h2>
          <CounteButton/>
      </div>
    );
  }
}

export default App;
